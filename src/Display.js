import React from 'react';
import './App.css';
class Display extends React.Component {
  render() {
    return (
      <div className="disp">
        <h1>{this.props.flag}</h1>
      </div>
    )
  }
}

export default Display;