import React from 'react';
import './App.css';
class Button extends React.Component {
  render() {
    return (
      <button className="button" onClick={this.props.handleClick}>{this.props.flag}</button>
    )
  }
}

export default Button;