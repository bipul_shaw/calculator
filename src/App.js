import React, { Component } from 'react';
import './App.css';
import Button from './Button'
import Display from './Display'

class App extends Component {
  state = { x: "0", last: false, paranOn: 0 };

  exp = (n) => {
    let current = false;
    if (n == "." || n == "+" || n == "-" || n == "*" || n == "/")
      current = true;
    if (this.state.x != "0") {
      if (current == true && this.state.last == true) {
        alert("First enter any value");
      }
      else {
        if (n == "." || n == "+" || n == "-" || n == "*" || n == "/") {
          this.setState((prevState) => ({ x: prevState.x + n, last: true }));
        }
        else {
          this.setState((prevState) => ({ x: prevState.x + n, last: false }));
        }
      }
    }
    else {
      if (n != "/" && n != "*" && n != "+" && n != "-" && n != ".")
        this.setState({ x: n, last: false });
      else
        this.setState((prevState) => ({ x: prevState.x + n, last: true }));
    }
  }

  reset = () => {
    this.setState({ x: "0" });
  }

  answer = () => {
    if (this.state.last == false) {
      this.setState((prevState) => ({ x: String(eval(prevState.x)) }));
    }
    else
      alert("First enter a value");
  }

  delete = () => {
    let c, s = this.state.x;
    if (s.length > 1) {
      c = s.slice(0, s.length - 1);
      this.setState({ x: c });
    }
    else {
      this.reset();
    }
  }

  render() {
    return (
      <div>
        <Display flag={this.state.x} />
        <div><Button flag={"1"} handleClick={() => this.exp("1")} />
          <Button flag={"2"} handleClick={() => this.exp("2")} />
          <Button flag={"3"} handleClick={() => this.exp("3")} /></div>
        <div><Button flag={"4"} handleClick={() => this.exp("4")} />
          <Button flag={"5"} handleClick={() => this.exp("5")} />
          <Button flag={"6"} handleClick={() => this.exp("6")} /></div>
        <div><Button flag={"7"} handleClick={() => this.exp("7")} />
          <Button flag={"8"} handleClick={() => this.exp("8")} />
          <Button flag={"9"} handleClick={() => this.exp("9")} /></div>
        <div><Button flag={"C"} handleClick={() => this.delete()} />
          <Button flag={"0"} handleClick={() => this.exp("0")} />
          <Button flag={"."} handleClick={() => this.exp(".")} /></div>
        <div><Button flag={"+"} handleClick={() => this.exp("+")} />
          <Button flag={"-"} handleClick={() => this.exp("-")} />
          <Button flag={"*"} handleClick={() => this.exp("*")} /></div>
        <div><Button flag={"/"} handleClick={() => this.exp("/")} />
          <Button flag={"="} handleClick={() => this.answer()} />
          <Button flag={"AC"} handleClick={() => this.reset()} /></div>
      </div>
    );
  }
}

export default App;